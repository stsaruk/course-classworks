/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

// window.addEventListener('load', function(){
//   let sliderContainer = document.getElementById('slider');
//   let nextButton = document.getElementById('NextSlide');
//   let prevButton = document.getElementById('PrevSlide');
//   let slideImage = document.createElement('img');
//       slideImage.classList.add('slider-image');
//   let ourSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
//   let currentPosition = 0;
//
//   function nextSlider() {
//     if (currentPosition === (ourSliderImages.length - 1)) {
//       currentPosition = 0;
//     } else {
//       currentPosition += 1;
//     }
//     renderImage(currentPosition);
//   }
//
//   function prevSlider() {
//     if (currentPosition === 0) {
//       currentPosition = (ourSliderImages.length - 1);
//     } else {
//       currentPosition -= 1;
//     }
//     renderImage(currentPosition);
//   }
//
//   function renderImage(currentPosition){
//     slideImage.src = ourSliderImages[currentPosition];
//     sliderContainer.appendChild(slideImage);
//   }
//   renderImage(currentPosition);
//
//
//   nextButton.addEventListener('click', nextSlider);
//   prevButton.addEventListener('click', prevSlider);
//
//
// });

// ----------------------------- method number 2 with transform animation -------------------------------

window.addEventListener('load', function(){
  let sliderContainer = document.getElementById('slider');
  let nextButton = document.getElementById('NextSlide');
  let prevButton = document.getElementById('PrevSlide');
  let ourSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
  let currentPosition = 0;
  let totalSlideWidth = 0;
  let slideWidth;
  let currentSlideWidth = 0;

  ourSliderImages.forEach(function (image, index) {

    let slideImage = document.createElement('img');
        slideImage.classList.add('slider-image');
        slideImage.src = ourSliderImages[index];
    sliderContainer.appendChild(slideImage);

    return totalSlideWidth = slideImage.offsetWidth * (ourSliderImages.length) ;
  });

  sliderContainer.style.width = totalSlideWidth + 'px';
  slideWidth = totalSlideWidth / (ourSliderImages.length);

  function nextSlider() {
    if (currentPosition === (ourSliderImages.length - 1)) {
      currentPosition = 0;
      currentSlideWidth = 0;
      sliderContainer.style.transform = 'translateX(' + '-' + currentSlideWidth + 'px)';
    } else {
      currentPosition += 1;
      currentSlideWidth += slideWidth;
      sliderContainer.style.transform = 'translateX(' + '-' + currentSlideWidth + 'px)';

    }
  }

  function prevSlider() {
    if (currentPosition === 0) {
      currentPosition = (ourSliderImages.length - 1);
      currentSlideWidth = totalSlideWidth - slideWidth;
      sliderContainer.style.transform = 'translateX(' + '-' + currentSlideWidth + 'px)';
    } else {
      currentPosition -= 1;
      currentSlideWidth -= slideWidth;
      sliderContainer.style.transform = 'translateX(' + '-' + currentSlideWidth + 'px)';
    }

  }

  nextButton.addEventListener('click', nextSlider);
  prevButton.addEventListener('click', prevSlider);


});

// ----------------------------- method number 3 ---------------------------------------------

// window.addEventListener('load', function(){
//   let sliderContainer = document.getElementById('slider');
//   let controlsContainer = document.getElementById('SliderControls');
//   let controlButtons = controlsContainer.querySelectorAll('button');
//   let slideImage = document.createElement('img');
//   let ourSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
//   let currentPosition = 0;
//
//
//   function renderImage(currentPosition){
//     slideImage.src = ourSliderImages[currentPosition];
//     sliderContainer.appendChild(slideImage);
//   }
//   renderImage(currentPosition);
//
//
//   controlButtons.forEach(function (button) {
//
//     button.addEventListener('click', function (e) {
//       var currentButton = e.target.getAttribute('id');
//
//       switch (currentButton) {
//         case 'NextSlide':
//           if (currentPosition === (ourSliderImages.length - 1)) {
//             currentPosition = 0;
//           } else {
//             currentPosition += 1;
//           }
//
//           renderImage(currentPosition);
//           break;
//         case 'PrevSlide':
//           if (currentPosition === 0) {
//             currentPosition = (ourSliderImages.length - 1);
//           } else {
//             currentPosition -= 1;
//           }
//
//           renderImage(currentPosition);
//           break;
//       }
//
//     });
//   });
//
// });

